<?php

/**
 * Process files using 'suitcss'.
 */
function _suitcss_prepro_compile($file) {
  $suitcssCLI = new suitcssCLI($file);

  $dependencies[] = $file;

  // SUITCSS CLI does not offer to show dependencies for a given file, so
  // revert to default mechanism
  _custom_prepro_find_dependencies($file, $dependencies);
  _custom_prepro_cache_dependencies($file, $dependencies);

  return array(
    $suitcssCLI->compile(),
    $suitcssCLI->get_error(),
  );
}
